/**
 * Clase la cual representa una instancia de clase foranea
 */

package Table.FKey;

import java.util.LinkedList;

import Table.Index.Index;

public class FKey {

	private String nameFK;
	private String nameTableRef;
	private LinkedList<String> nameColumnFK;
	private LinkedList<String> nameColumnPK;
	
	public FKey() {
		this.nameFK = null;
		this.nameTableRef = null;
		this.nameColumnFK = null;
		this.nameColumnPK = null;
	}
	
	public String getNameFK() {
		return this.nameFK;
	}
	
	public void setNameFK(String newNameFK) {
		this.nameFK = newNameFK;
	}
	
	public String getNameTableRef() {
		return this.nameTableRef;
	}
	
	public void setNameTableRef(String newNameTableRef) {
		this.nameTableRef = newNameTableRef;
	}
	
	public LinkedList<String> getNameColumnFK() {
		return this.nameColumnFK;
	}
	
	public void setNameColumnFK(LinkedList<String> newNameColumnFK) {
		this.nameColumnFK = newNameColumnFK;
	}
	
	public LinkedList<String> getNameColumnPK() {
		return this.nameColumnPK;
	}
	
	public void setNameColumnPK(LinkedList<String> newNameColumnPK) {
		this.nameColumnPK = newNameColumnPK;
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o instanceof FKey) {
    		FKey other = (FKey) o;
    		if (this.nameFK.equals(other.nameFK)) {
    			if (this.nameTableRef.equals(other.nameTableRef)) {
    				if (this.nameColumnFK.size() == other.nameColumnFK.size()) {
    					for (int i = 0; i < this.nameColumnFK.size(); i++) {
    						if (!this.nameColumnFK.get(i).equals(other.nameColumnFK.get(i))) return false;
    					}
    					if (this.nameColumnPK.size() == other.nameColumnPK.size()) {
    						for (int i = 0; i < this.nameColumnPK.size(); i++) {
    							if (!this.nameColumnPK.get(i).equals(other.nameColumnPK.get(i))) return false;
    						}
    						
    						return true;
    					}
    					
    					return false;
    				}
    				
    				return false;
    			}
    			
    			return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
	}
}
