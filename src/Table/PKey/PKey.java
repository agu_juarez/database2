package Table.PKey;

import java.util.LinkedList;

import Table.Index.Index;

public class PKey {

	private LinkedList<String> setName;
	
	public PKey() {
		this.setName = null;
	}
	
	public LinkedList<String> getSetName() {
		return this.setName;
	}
	
	public void setSetName(LinkedList<String> newSetName) {
		this.setName = newSetName;
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o instanceof PKey) {
    		PKey other = (PKey) o;
    		if (this.setName.size() == other.setName.size()) {
    			for (int i = 0; i < this.setName.size(); i++) {
    				if (!this.setName.get(i).equals(other.setName.get(i))) return false;
    			}
    			
    			return true;
    		}
    		
    		return false;
    	}
    	
    	return false;
	}
}
