/**
 * Clase la cual representa una instancia de tabla
 */

package Table;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import Table.FKey.FKey;
import Table.Index.Index;
import Table.Column.Column;
import Table.PKey.PKey;
import Table.Trigger.Trigger;

public class Table {

    private String name;
    private LinkedList<Column>  setColumn;
    private LinkedList<PKey>  setPrimaryKeys;
    private LinkedList<String>  setUniqueKeys;
    private LinkedList<FKey>    setForeignKey;
    private LinkedList<Index>   setIndexs;
    private LinkedList<Trigger> setTrigger;
    
    public Table() {
        this.name = null;
        this.setColumn = null;
        this.setPrimaryKeys = null;
        this.setUniqueKeys = null;
        this.setIndexs = null;
        this.setTrigger = null;
    }

    public Table(String nameTable, LinkedList<Column> setColumn, LinkedList<Trigger> setTrigger, LinkedList<PKey> setPrimaryKeys, LinkedList<String> setUniqueKeys, LinkedList<FKey> setForeignKey,LinkedList<Index> setIndexs) {
        this.name = nameTable;
        this.setColumn = setColumn;
        this.setPrimaryKeys = setPrimaryKeys;
        this.setUniqueKeys = setUniqueKeys;
        this.setForeignKey = setForeignKey;
        this.setIndexs = setIndexs;
        this.setTrigger = setTrigger;
    }

    public String getNameTable() {
        return this.name;
    }

    public void setNameTable(String newNameTable) {
        this.name = newNameTable;
    }

    public LinkedList<Column> getSetColumn() {
        return this.setColumn;
    }

    public void setSetColumn(LinkedList<Column> setColumn) {
        this.setColumn = setColumn;
    }

    public LinkedList<PKey> getSetPrimaryKeys() {
        return this.setPrimaryKeys;
    }

    public void setSetPrimaryKeys(LinkedList<PKey> setPrimaryKeys) {
        this.setPrimaryKeys = setPrimaryKeys;
    }

    public LinkedList<String> getSetUniqueKeys() {
        return this.setUniqueKeys;
    }

    public void setSetUniqueKeys(LinkedList<String> setUniqueKeys) {
        this.setUniqueKeys = setUniqueKeys;
    }
    
    public LinkedList<FKey> getSetForeingKeys() {
    	return this.setForeignKey;
    }
    
    public void setSetForeingKeys(LinkedList<FKey> newSetForeignKey) {
    	this.setForeignKey = newSetForeignKey;
    }
    
    public LinkedList<Index> getSetIndexs() {
    	return this.setIndexs;
    }
    
    public void setSetIndexs(LinkedList<Index> newSetIndexs) {
    	this.setIndexs = newSetIndexs;
    }

    public LinkedList<Trigger> getSetTrigger() {
        return this.setTrigger;
    }

    public void setSetTrigger(LinkedList<Trigger> setTrigger) {
        this.setTrigger = setTrigger;
    }
    
    private boolean notEqualPrimaryKeys(Table other) {
    	for (PKey pk : this.setPrimaryKeys) {
    		if (!other.setPrimaryKeys.contains(pk)) return true;
    	}
    	
    	for (PKey pk : other.setPrimaryKeys) {
    		if (!this.setPrimaryKeys.contains(pk)) return true;
    	}
    	
    	return false;
    }
    
    private boolean notEqualUniqueKeys(Table other) {
    	for (String s : this.setUniqueKeys) {
    		if (!other.setUniqueKeys.contains(s)) return true;
    	}
    	
    	for (String s : other.setUniqueKeys) {
    		if (!this.setUniqueKeys.contains(s)) return true;
    	}
    	
    	return false;
    }
    
    private boolean existColumnsNotEqualsName(Table other) {
		LinkedList<String> nameColumns = new LinkedList<String>();
		
		for (Column c1: other.setColumn) {
			nameColumns.add(c1.getNameColumn());
		}
			
		for (Column c2 : this.setColumn) {
			if (!nameColumns.contains(c2.getNameColumn())) return true;
		}
			
		nameColumns.clear();
		
		for (Column c1: this.setColumn) {
			nameColumns.add(c1.getNameColumn());
		}
		
		for (Column c2 : other.setColumn) {
			if (!nameColumns.contains(c2.getNameColumn())) return true;
		}
		
		return false;
    }
    
    private boolean existColumnsEqualsNameDiffType(Table other) {
    	for (Column c1 : this.setColumn) {
    		for (Column c2 : other.setColumn) {
    			if (c1.getNameColumn().equals(c2.getNameColumn()) && !c1.equals(c2))
    				return true;
    		}
    	}
    	
    	return false;
    }
    
    private boolean existTriggersNotEqualsName(Table other) {
    	LinkedList<String> nameTriggers = new LinkedList<String>();
    	
    	for (Trigger t1: other.setTrigger) {
			nameTriggers.add(t1.getNameTrigger());
		}
			
		for (Trigger t2 : this.setTrigger) {
			if (!nameTriggers.contains(t2.getNameTrigger())) return true;
		}
			
		nameTriggers.clear();
		
		for (Trigger t1: this.setTrigger) {
			nameTriggers.add(t1.getNameTrigger());
		}
		
		for (Trigger t2 : other.setTrigger) {
			if (!nameTriggers.contains(t2.getNameTrigger())) return true;
		}
		
		return false;
    }
    
    private boolean existIndexNotEqualsName(Table other) {
    	LinkedList<String> nameIndexs = new LinkedList<String>();
    	
    	for (Index i1: other.setIndexs) {
			nameIndexs.add(i1.getNameIndex());
		}
    	
		for (Index i2 : this.setIndexs) {
			if (!nameIndexs.contains(i2.getNameIndex())) return true;
		}
			
		nameIndexs.clear();
		
		for (Index i1: this.setIndexs) {
			nameIndexs.add(i1.getNameIndex());
		}
		
		for (Index i2 : other.setIndexs) {
			if (!nameIndexs.contains(i2.getNameIndex())) return true;
		}
		
		return false;
    }
    
    private boolean existIndexsEqualsNameDiff(Table other) {
    	for (Index i1 : this.setIndexs) {
    		for (Index i2 : other.setIndexs) {
    			if (i1.getNameIndex().equals(i2.getNameIndex()) && !i1.equals(i2)) return true;
    		}
    	}
    	
    	return false;
    }
    
    private boolean existFKNotEqualsName(Table other) {
    	LinkedList<String> nameFK = new LinkedList<String>();
    	
    	for (FKey f1: other.setForeignKey) {
			nameFK.add(f1.getNameFK());
		}
			
		for (FKey f2 : this.setForeignKey) {
			if (!nameFK.contains(f2.getNameFK())) return true;
		}
			
		nameFK.clear();
		
		for (FKey f1: this.setForeignKey) {
			nameFK.add(f1.getNameFK());
		}
		
		for (FKey f2 : other.setForeignKey) {
			if (!nameFK.contains(f2.getNameFK())) return true;
		}
		
		return false;
    }
    
    private boolean existFKEqualsNameDiff(Table other) {
    	for (FKey f1 : this.setForeignKey) {
    		for (FKey f2 : other.setForeignKey) {
    			if (f1.getNameFK().equals(f2.getNameFK()) && !f1.equals(f2)) return true;
    		}
    	}
    	
    	return false;
    }
     
    private boolean existTriggersEqualsNameDiffTCD(Table other) {
    	for (Trigger t1 : this.setTrigger) {
    		for (Trigger t2 : other.setTrigger) 
    			if (t1.getNameTrigger().equals(t2.getNameTrigger()) && !t1.equals(t2)) return true;
    	}
    	
    	return false;
    }
    
    private String generate(LinkedList<String> set) {
    	String result = "";
    	
    	for (String str : set) {
    		result = result + " " + str;
    	}
    	
    	return result;
    }
    
    public void diffTablesEqualName(Table other) {
    	System.out.println("   " + " ---------- Diferencia entre las tablas " + this.getNameTable() + "----------" + "\n");
    	
    	//CHEQUEA SI HAY COLUMNAS CON DIFERENTE NOMBRES
    	if (this.existColumnsNotEqualsName(other)) {
	    	System.out.println("-> EXISTEN DIFERENCIAS EN LAS COLUMNAS\n");
				
			LinkedList<String> nameColumns = new LinkedList<String>();
			for (Column c1: other.setColumn) {
				nameColumns.add(c1.getNameColumn());
			}
				
			for (Column c2 : this.setColumn) {
				if (!nameColumns.contains(c2.getNameColumn()))
					System.out.println("   " + " - COLUMNA ADICIONAL: " + c2.getNameColumn() + " EN LA TABLA: " + this.getNameTable() + " EN LA PRIMER BD\n");
			}
				
			nameColumns.clear();
			for (Column c1: this.setColumn) {
				nameColumns.add(c1.getNameColumn());
			}
		
			for (Column c2 : other.setColumn) {
				if (!nameColumns.contains(c2.getNameColumn()))
					System.out.println("   " + " - COLUMNA ADICIONAL: " + c2.getNameColumn() + " EN LA TABLA: " + other.getNameTable() + " EN LA SEGUNDA BD\n");
			}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN LAS COLUMNAS, SON TODAS IGUALES EN CUANTO A NOMBRE \n");
    	}
		
		//CHQUEA SI HAY DIFERENCIA DE TIPOS EN COLUMNAS DEL MISMO NOMBRE
		if (this.existColumnsEqualsNameDiffType(other)) {
			System.out.println("-> EXISTEN COLUMNAS CON EL MISMO NOMBRE PERO DIFERENTE TIPO \n");
			System.out.println("   " + "-> ELLAS SON: " + " \n");
			for (Column c1 : this.setColumn) {
	    		for (Column c2 : other.setColumn) {
	    			if (c1.getNameColumn().equals(c2.getNameColumn()) && !c1.equals(c2)) {
	    				System.out.println("   " + " - NOMBRE TABLA: " + this.getNameTable()  + " NOMBRE COLUMNA: " + c1.getNameColumn() + " TIPO COLUMNA: " + c1.getTypeColumn() + " EN LA PRIMER BD" + "\n");
    					System.out.println("   " + " - NOMBRE TABLA: " + other.getNameTable() + " NOMBRE COLUMNA : " + c2.getNameColumn() + " TIPO COLUMNA : " + c2.getTypeColumn() + " EN LA SEGUNDA BD" + "\n");
	    			}
	    		}
	    	}
		} else {
			System.out.println("-> NO EXISTE DIFERENCIA DE TIPOS EN COLUMNAS DE IGUAL NOMBRE \n");
		} 
		
    	
    	//CHEQUEA SI HAY DIFERENCIA ENTRE CLAVES PRIMARIAS
    	if (this.notEqualPrimaryKeys(other)) {
    		System.out.println("-> EXISTEN DIFERENCIA EN LAS CLAVES PRIMARIAS \n");
    		System.out.println("-> LAS CLAVES PRIMARIAS DIFERENTE SON: \n");
    		for (PKey pk : this.setPrimaryKeys) {
    			if (!other.setPrimaryKeys.contains(pk)) {
    				System.out.println("   " + " - NOMBRE DE LA TABLA: " + this.getNameTable() + " CLAVE PRIMARIA: " + generate(pk.getSetName()) + " EN LA PRIMER BD \n");
    			}
    		}
    		for (PKey pk : other.setPrimaryKeys) {
    			if (!this.setPrimaryKeys.contains(pk)) {
    				System.out.println("   " + " - NOMBRE DE LA TABLA: " + other.getNameTable() + " CLAVE PRIMARIA: " + generate(pk.getSetName()) + " EN LA SEGUNDA BD \n");
    			}
    		}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN LAS CLAVES PRIMARIAS, SON LAS MISMAS \n");
    	}
    	
    	//CHEQUEA SI HAY DIFERENCIA ENTRE CLAVES UNICAS
    	if (this.notEqualUniqueKeys(other)) {
    		System.out.println("-> EXISTEN DIFERENCIA EN LAS CLAVES UNICAS \n");
    		System.out.println("-> LAS CLAVES UNICAS DIFERENTE SON: \n");
    		for (String s1 : this.setUniqueKeys) {
    			if (!other.setUniqueKeys.contains(s1)) {
    				System.out.println("   " + " - NOMBRE DE LA TABLA: " + this.getNameTable() + " CLAVE UNICA: " + s1.toString() + " EN LA PRIMER BD \n");
    			}
    		}
    		for (String s1 : other.setUniqueKeys) {
    			if (!this.setUniqueKeys.contains(s1)) {
    				System.out.println("   " + " - NOMBRE DE LA TABLA: " + other.getNameTable() + " CLAVE UNICA: " + s1.toString() + " EN LA SEGUNDA BD \n");
    			}
    		}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN LAS CLAVES UNICAS, SON LAS MISMAS \n");
    	}
    	
    	//CHEQUEA SI HAY CLAVES FORANEAS CON DISTINTO NOMBRES
    	if (this.existFKNotEqualsName(other)) {
    		System.out.println("-> EXISTEN CLAVES FORANEAS CON DIFERENTES NOMBRES\n");
    		
    		LinkedList<String> nameFK = new LinkedList<String>();
			for (FKey f1: other.setForeignKey) {
				nameFK.add(f1.getNameFK());
			}
				
			for (FKey f2 : this.setForeignKey) {
				if (!nameFK.contains(f2.getNameFK()))
					System.out.println("   " + " - CLAVE FORANEA ADICIONAL: " + f2.getNameFK() + " EN LA TABLA: " + this.getNameTable() + " EN LA PRIMER BD \n");
			}
				
			nameFK.clear();
			for (FKey f1: this.setForeignKey) {
				nameFK.add(f1.getNameFK());
			}
				
			for (FKey f2 : other.setForeignKey) {
				if (!nameFK.contains(f2.getNameFK()))
					System.out.println("   " + " - CLAVE FORANEA ADICIONAL: " + f2.getNameFK() + " EN LA TABLA: " + other.getNameTable() + " EN LA SEGUNDA BD\n");
			}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN LAS CLAVES FORANEAS, SON TODAS IGUALES EN CUANTO A NOMBRE \n");
    	}
    	
    	//CHEQUEA SI EXISTEN CLAVES FORANEAS CON EL MISMO NOMBRE PERO DIFERENTES
    	if (this.existFKEqualsNameDiff(other)) {
    		System.out.println("-> EXISTEN CLAVES FORANEAS CON IGUAL NOMBRE PERO DISTINTAS\n");
    		System.out.println("   " + "-> ELLAS SON: " + " \n");
    		
    		for (FKey f1 : this.setForeignKey) {
    			for (FKey f2 : other.setForeignKey) {
    				if (f1.getNameFK().equals(f2.getNameFK()) && !f1.equals(f2)) {
    					System.out.println("   " + " - NOMBRE TABLA: " + this.getNameTable() + " NOMBRE CLAVE FORANEA: " + f1.getNameFK() + " TABLA A LA QUE HACE REFERENCIA: " + f1.getNameTableRef() + " NOMBRE DE LAS COLUMNAS DE LA CLAVE PRINCIPAL: " + generate(f1.getNameColumnPK()) + " NOMBRE DE LAS COLUMNAS DE CLAVE EXTERNA: " + generate(f1.getNameColumnFK()) + " EN LA PRIMER BD \n");
    					System.out.println("   " + " - NOMBRE TABLA: " + other.getNameTable() + " NOMBRE CLAVE FORANEA: " + f2.getNameFK() + " TABLA A LA QUE HACE REFERENCIA: " + f2.getNameTableRef() + " NOMBRE DE LAS COLUMNAS DE LA CLAVE PRINCIPAL: " + generate(f2.getNameColumnPK()) + " NOMBRE DE LAS COLUMNAS DE CLAVE EXTERNA: " + generate(f2.getNameColumnFK()) + " EN LA SEGUNDA BD \n");
    				}
    			}
    		}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN FK DE IGUAL NOMBRE, TODAS REFERENCIAN A LA MISMA TABLA\n");
    	}
    	
    	//CHEQUEA SI HAY INDICES CON DIFERENTES NOMBRES
    	if (this.existIndexNotEqualsName(other)) {
    		System.out.println("-> EXISTEN INDICES CON DIFERENTE NOMBRES \n");
    		
			LinkedList<String> nameIndexs = new LinkedList<String>();
			for (Index i1: other.setIndexs) {
				nameIndexs.add(i1.getNameIndex());
			}
			
			for (Index i2 : this.setIndexs) {
				if (!nameIndexs.contains(i2.getNameIndex()))
					System.out.println("   " + " - INDICE ADICIONAL: " + i2.getNameIndex() + " ES UNICO?: " + i2.getIsUnique() + " EN LAS COLUMNAS: " + generate(i2.getNameColumns()) + " EN LA TABLA: " + this.getNameTable() + " EN LA PRIMER BD\n");
			}
			
			nameIndexs.clear();
			for (Index i1: this.setIndexs) {
				nameIndexs.add(i1.getNameIndex());
			}

			for (Index i2 : other.setIndexs) {
				if (!nameIndexs.contains(i2.getNameIndex()))
					System.out.println("   " + " - INDICE ADICIONAL: " + i2.getNameIndex() + " ES UNICO?: " + i2.getIsUnique() + " EN LAS COLUMNAS: " + generate(i2.getNameColumns()) + " EN LA TABLA: " + this.getNameTable() + " EN LA SEGUNDA BD\n");
			}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN LOS INDICES, SON TODOS IGUALES EN CUANTO A NOMBRE \n");
    	}
    	
    	//CHEQUES SI HAY INDICES CON IGUAL NOMBRE PERO DIFERENTES
    	if (this.existIndexsEqualsNameDiff(other)) {
			System.out.println("-> EXISTEN INDICES CON EL MISMO NOMBRE PERO QUE SON DIFERENTES \n");
			System.out.println("   " + "-> ELLOS SON: " + " \n");
			
			for (Index i1 : this.setIndexs) {
	    		for (Index i2 : other.setIndexs) {
	    			if (i1.getNameIndex().equals(i2.getNameIndex()) && !i1.equals(i2)) {
	    				System.out.println("   " + " - INDICE ADICIONAL: " + i1.getNameIndex() + " ES UNICO?: " + i1.getIsUnique() + " EN LAS COLUMNAS: " + generate(i1.getNameColumns()) + " EN LA TABLA: " + this.getNameTable() + " EN LA PRIMER BD\n");
	    				System.out.println("   " + " - INDICE ADICIONAL: " + i2.getNameIndex() + " ES UNICO?: " + i2.getIsUnique() + " EN LAS COLUMNAS: " + generate(i2.getNameColumns()) + " EN LA TABLA: " + other.getNameTable() + " EN LA SEGUNDA BD\n");	    			}
	    		}
	    	}
    	} else {
    		System.out.println("-> NO EXISTE DIFERENCIA EN LOS INDICES DE IGUAL NOMBRE \n");
    	}
    	
		//CHEQUEA SI HAY TRIGGERS CON DIFERENTE NOMBRE
		if (this.existTriggersNotEqualsName(other)) {
			System.out.println("-> EXISTEN TRIGGER CON DIFERENTE NOMBRES \n");
			
			LinkedList<String> nameTriggers = new LinkedList<String>();
			for (Trigger t1: other.setTrigger) {
				nameTriggers.add(t1.getNameTrigger());
			}
				
			for (Trigger t2 : this.setTrigger) {
				if (!nameTriggers.contains(t2.getNameTrigger()))
					System.out.println("   " + " - TRIGGER ADICIONAL: " + t2.getNameTrigger() + " EN LA TABLA: " + this.getNameTable() + " EN LA PRIMER BD\n");
			}
				
			nameTriggers.clear();
			for (Trigger t1: this.setTrigger) {
				nameTriggers.add(t1.getNameTrigger());
			}

			for (Trigger t2 : other.setTrigger) {
				if (!nameTriggers.contains(t2.getNameTrigger()))
					System.out.println("   " + " - TRIGGER ADICIONAL: " + t2.getNameTrigger() + " EN LA TABLA: " + other.getNameTable() + " EN LA PRIMER BD\n");
			}
		} else {
			System.out.println("-> NO EXISTE DIFERENCIA EN LOS TRIGGERS, SON TODOS IGUALES EN CUANTO A NOMBRE \n");
		}
		
		//CHEQUEA SI HAY DIFERENCIA EN LA CONDICION DE DISPARO EN TRIGGERS CON EL MISMO NOMBRE
		if (this.existTriggersEqualsNameDiffTCD(other)) {
			System.out.println("-> EXISTEN TRIGGERS CON EL MISMO NOMBRE PERO DIFERENTE CONDICION DE DISPARO \n");
			System.out.println("   " + "-> ELLOS SON: " + " \n");
			for (Trigger t1 : this.setTrigger) {
	    		for (Trigger t2 : other.setTrigger) {
	    			if (t1.getNameTrigger().equals(t2.getNameTrigger()) && !t1.equals(t2)) {
	    				System.out.println("   " + " - NOMBRE TABLA: " + this.getNameTable()  + " NOMBRE DEL TRIGGER: " + t1.getNameTrigger() + " CONDICION DE DISPARO: " + t1.getMomentTrigger() + " EVENTO: " + t1.getEventTrigger() + " EN LA PRIMER BD" + "\n");
    					System.out.println("   " + " - NOMBRE TABLA: " + other.getNameTable() + " NOMBRE DEL TRIGGER : " + t2.getNameTrigger() + " CONDICION DE DISPARO : " + t2.getMomentTrigger() + " EVENTO: " + t2.getEventTrigger() + " EN LA SEGUNDA BD" + "\n");
	    			}
	    		}
	    	}
		} else {
			System.out.println("-> NO EXISTE DIFERENCIA EN LA CONDICION DE DISPARO DE TRIGGERS CON EL MISMO NOMBRE \n");
		}
    } 
    
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Table) {
    		Table other = (Table) o;
    		if (this.name.equals(other.name)) {
    			if (this.setColumn.size() == other.setColumn.size()) {
    				for (Column c1 : this.setColumn) 
    					if (!other.setColumn.contains(c1)) return false;
    				for (Column c2 : other.setColumn) 
    					if (!this.setColumn.contains(c2)) return false;
    				
    				if (this.setPrimaryKeys.size() == other.setPrimaryKeys.size()) {
    					for (PKey pk : this.setPrimaryKeys) 
    						if (!other.setPrimaryKeys.contains(pk)) return false;
    					for (PKey pk : other.setPrimaryKeys) 
    						if (!this.setPrimaryKeys.contains(pk)) return false;
    					
    					if (this.setUniqueKeys.size() == other.setUniqueKeys.size()) {
        					for (String uK1 : this.setUniqueKeys) 
        						if (!other.setUniqueKeys.contains(uK1)) return false;
        					for (String uK2 : other.setUniqueKeys) 
        						if (!this.setUniqueKeys.contains(uK2)) return false;
        					
    						if (this.setForeignKey.size() == other.setForeignKey.size()) {
    	    					for (FKey fK1 : this.setForeignKey) 
    	    						if (!other.setForeignKey.contains(fK1)) return false;
    	    					for (FKey fK2 : other.setForeignKey) 
    	    						if (!this.setForeignKey.contains(fK2)) return false;
    	    					
	    						if (this.setIndexs.size() == other.setIndexs.size()) {
	    	    					for (Index index1 : this.setIndexs) 
	    	    						if (!other.setIndexs.contains(index1)) return false;
	    	    					for (Index index2 : other.setIndexs) 
	    	    						if (!this.setIndexs.contains(index2)) return false;
	    	    					
		    						if (this.setTrigger.size() == other.setTrigger.size()) {
		    	    					for (Trigger tri1 : this.setTrigger) 
		    	    						if (!other.setTrigger.contains(tri1)) return false;
		    	    					for (Trigger tri2 : other.setTrigger) 
		    	    						if (!this.setTrigger.contains(tri2)) return false;
		    							
		    							return true;
		    						}
		    						
		    						return false;
	    						}
	    						
	    						return false;
    						}
    						
    						return false;
    					}
    					
    					return false;
    				}
    				
    				return false;
    			}
    			
    			return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
    }
    
	
}
