/**
 * Clase la cual representa una instancia de Columna
 */

package Table.Column;

import java.util.*;

public class Column {

    private String name;
    private String type;

    public Column() {
        this.name = null;
        this.type = null;
    }

    public Column(String nameColumn, String typeColumn) {
        this.name = nameColumn;
        this.type = typeColumn;
    }

    public String getNameColumn() {
        return this.name;
    }

    public void setNameColumn(String newNameColumn) {
        this.name = newNameColumn;
    }

    public String getTypeColumn() {
        return this.type;
    }

    public void setTypeColumn(String newTypeColumn) {
        this.type = newTypeColumn;
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Column) {
    		Column other = (Column) o;
    		if (this.name.equals(other.name)) {
    			if (this.type.equals(other.type))
    				return true;
    			else
    				return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
    }
}
