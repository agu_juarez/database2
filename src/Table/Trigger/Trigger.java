/**
 * Clase la cual representa una instancia de Trigger
 */

package Table.Trigger;

import java.util.*;

import Table.Column.Column;

public class Trigger {

    private String name;
    private String momentTrigger;
    private String eventTrigger;

    public Trigger() {
        this.name = null;
        this.momentTrigger = null;
        this.eventTrigger = null;
    }

    public Trigger(String nameTrigger, String momentTrigger, String eventTrigger) {
        this.name = nameTrigger;
        this.momentTrigger = momentTrigger;
        this.eventTrigger = eventTrigger;
    }

    public String getNameTrigger() {
        return this.name;
    }

    public void setNameTrigger(String newNameTrigger) {
        this.name = newNameTrigger;
    }

    public String getMomentTrigger() {
        return this.momentTrigger;
    }

    public void setMomentTrigger(String newMomentTrigger) {
        this.momentTrigger = newMomentTrigger;
    }
    
    public String getEventTrigger() {
    	return this.eventTrigger;
    }
    
    public void setEventTrigger(String newEventTrigger) {
    	this.eventTrigger = newEventTrigger;
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Trigger) {
    		Trigger other = (Trigger) o;
    		if (this.name.equals(other.name)) {
    			if (this.momentTrigger.equals(other.momentTrigger)) {
    				if (this.eventTrigger.equals(other.eventTrigger)) return true;
    				return false;
    			}
    			
    			return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
    }
}
