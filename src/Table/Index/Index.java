/**
 * Clase la representa una instancia de Indice
 */

package Table.Index;

import java.util.LinkedList;

import Table.Table;
import Table.Column.Column;

public class Index {
	
	private String name;
	private boolean isUnique;
	private LinkedList<String> nameColumns;
	
	public Index() {
		this.name = null;
		this.isUnique = false;
		this.nameColumns = null;
	}

	public String getNameIndex() {
		return this.name;
	}
	
	public void setNameIndex(String newNameIndex) {
		this.name = newNameIndex;
	}
	
	public boolean getIsUnique() {
		return this.isUnique;
	}
	
	public void setIsUnique(boolean isUnique) {
		this.isUnique = isUnique;
	}
	
	public LinkedList<String> getNameColumns() {
		return this.nameColumns;
	}
	
	public void setNameColumns(LinkedList<String> newNameColumns) {
		this.nameColumns = newNameColumns;
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o instanceof Index) {
    		Index other = (Index) o;
    		if (this.name.equals(other.name)) {
    			if (this.isUnique == other.isUnique) {
    				if (this.nameColumns.size() == other.nameColumns.size()) {
    					for (int i = 0; i < this.nameColumns.size(); i++) {
    						if (!this.nameColumns.get(i).equals(other.nameColumns.get(i))) return false;
    					}
    					
    					return true;
    				}
    				
    				return false;
    			}
    			
    			return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
    }
}
