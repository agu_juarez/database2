/**
 * Clase la cual contiene el metodo principal
 */

package Main;

import java.sql.*;
import java.util.*;

import Aux.checkStructure;
import Procedure.Procedure;
import Table.Table;
import Table.Column.Column;
import Table.Trigger.Trigger;
import Aux.Connect;
import Aux.Driver;

public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Driver.loadDriver();
        Connect co = new Connect();
        
        LinkedList<Table> setTablesDBOne = new LinkedList<Table>();
        LinkedList<Procedure> setproceduresDBOne = new LinkedList<Procedure>();
 
        LinkedList<Table> setTablesDBTwo = new LinkedList<Table>();
        LinkedList<Procedure> setproceduresDBTwo = new LinkedList<Procedure>();

        Connection connectionDBOne = null;
        Connection connectionDBTwo = null;
		char cont;
		do {
			Scanner sc = new Scanner(System.in);
			System.out.println("BIENVENIDO AL SISTEMA DE CHEQUEO DE ESTRUCTURAS DE DOS BD");
			System.out.println("OPERACIONES DISPONIBLES:");
			System.out.println("1- EJECUTAR EL SISTEMA CON LA CONECCION POR DEFECTO (Base datos dataBaseOne y dataBaseTwo)");
			System.out.println("2- EJECUTAR EL SISTEMA CON CONECCION DESEADA");
			System.out.println("INGRESE EL NUMERO DE OPERACION QUE DESEA REALIZAR");
			int option = sc.nextInt();
			switch(option){
				
			case 1:
				co.requestDataDefault("localhost:3306", "dataBaseOne", "root", "root");
		        connectionDBOne = co.establishConnection(connectionDBOne);
		        setTablesDBOne = co.generateSetTables(connectionDBOne, co.getNameDB());
		        setproceduresDBOne = co.generateSetProcedureAndFunction(connectionDBOne, co.getNameDB());
		        
		        co.requestDataDefault("localhost:3306", "dataBaseTwo", "root", "root");
		        connectionDBTwo = co.establishConnection(connectionDBTwo);
		        setTablesDBTwo = co.generateSetTables(connectionDBTwo, co.getNameDB());
		        setproceduresDBTwo = co.generateSetProcedureAndFunction(connectionDBTwo, co.getNameDB());
		        
		        checkStructure.checkTable(setTablesDBOne, setTablesDBTwo);
		        checkStructure.checkProcedure(setproceduresDBOne, setproceduresDBTwo);
			break;
			case 2:
		        System.out.println("INGRESE LOS DATOS PARA LA CONECCION A LA PRIMER DB\n\n");
		        co.requestData();
		        connectionDBOne = co.establishConnection(connectionDBOne);
		        setTablesDBOne = co.generateSetTables(connectionDBOne, co.getNameDB());
		        setproceduresDBOne = co.generateSetProcedureAndFunction(connectionDBOne, co.getNameDB());
		        
		        System.out.println("---------------------------------------------------------------------\n");
		        
		        System.out.println("INGRESE LOS DATOS PARA LA CONECCION A LA SEGUNDA DB\n\n");
		        co.requestData();
		        connectionDBTwo = co.establishConnection(connectionDBTwo);
		        setTablesDBTwo = co.generateSetTables(connectionDBTwo, co.getNameDB());
		        setproceduresDBTwo = co.generateSetProcedureAndFunction(connectionDBTwo, co.getNameDB());
		        
		        checkStructure.checkTable(setTablesDBOne, setTablesDBTwo);
		        checkStructure.checkProcedure(setproceduresDBOne, setproceduresDBTwo);
			break;
			}
		
			System.out.println("DESEA EJECUTAR OTRA OPCION (S/N)");
			cont = (char)sc.next().charAt(0);
		}while((cont == 83) || (cont == 115)); 
	}
}
