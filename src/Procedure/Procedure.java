/**
 * Clase la cual representa una instancia de un procedimiento
 */

package Procedure;

import java.util.*;

import Aux.Pair;

public class Procedure {

    private String name;
    private LinkedList<Pair> setParameter;
    private String typeReturn;

    public Procedure() {
        this.name = null;
        this.setParameter = null;
        this.typeReturn = null;
    }

    public String getNameProcedure() {
        return this.name;
    }

    public void setNameProdecure(String name) {
        this.name = name;
    }

    public LinkedList<Pair> getSetParameter() {
        return this.setParameter;
    }

    public void setSetParameter(LinkedList<Pair> setParameter) {
        this.setParameter = setParameter;
    }

    public String getTypeReturn() {
        return this.typeReturn;
    }

    public void setTypeReturn(String typeReturn) {
        this.typeReturn = typeReturn;        
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Procedure) {
    		Procedure other = (Procedure) o;
    		if (this.name.equals(other.name)) {
    			if (this.setParameter.size() == other.setParameter.size()) {
    				for(int i = 0; i < this.setParameter.size(); i++) {
    					if (!this.setParameter.get(i).equals(other.setParameter.get(i))) return false;
    				}
    				if (this.typeReturn != null && other.typeReturn != null) {
        				if (this.typeReturn.equals(other.typeReturn))
        					return true;
        				return false;
    				} else {
    					if (this.typeReturn == null && other.typeReturn == null)
    						return true;
    					return false;
    				}
    			}
    			
    			return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
    }
    
}
