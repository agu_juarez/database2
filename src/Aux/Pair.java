package Aux;

import Table.Table;

public class Pair {

	private String first;
	private String second;
	
	public Pair() {
		this.first = null;
		this.second = null;
	}
	
	public String getFirst() {
		return this.first;
	}
	
	public void setFirst(String newFirst) {
		this.first = newFirst;
	}
	
	public String getSecond() {
		return this.second;
	}
	
	public void setSecond(String newSecond) {
		this.second = newSecond;
	}
	
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Pair) {
    		Pair other = (Pair) o;
    		if (this.first.equals(other.first)) {
    			if (this.second.equals(other.second)) {
    				return true;
    			}
    			
    			return false;
    		}
    		
    		return false;
    	}
    	
    	return false;
    }
}
