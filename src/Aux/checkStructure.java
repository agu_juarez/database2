/**
 * Clase la cual es la encargada de chequear si dos BD tiene las mismas tablas y procedimientos, y informas las diferencias si es que existen
 */

package Aux;

import java.util.*;
import java.io.*;

import Table.Table;

import Procedure.*;

public class checkStructure {

	/**
	 * Metodo el cual dado un conjunto de parametros genera la secuencia de parametros (int, int)
	 * @param setParameters, conjunto de parametros
	 * @return String, secuencia de parametros
	 */
	private static String generateSequenceParameters(LinkedList<Pair> setParameters) {
		String sequenceParameters = "";
		
		for (Pair p : setParameters) {
			sequenceParameters = sequenceParameters + " " + p.getFirst() + " " + p.getSecond();
		}
		
		sequenceParameters = "(" + sequenceParameters + ")";
		return sequenceParameters;
	}
	
	/**
	 * Metodo el cual toma dos conjuntos de precedimientos y chequea si no hay diferencia entre ellos, o si las hay cuales son
	 * @param setProcedureO
	 * @param setProcedureT
	 */
	public static void checkProcedure(LinkedList<Procedure> setProcedureO, LinkedList<Procedure> setProcedureT) {
		LinkedList<Procedure> procedureCom = new LinkedList<Procedure>();
		LinkedList<Procedure> procedureEqualsName = new LinkedList<Procedure>();
        LinkedList<Procedure> aditionalDBOne = new LinkedList<Procedure>();
        LinkedList<Procedure> aditionalDBTwo = new LinkedList<Procedure>();
        
        System.out.println("---------- CHEQUEO NIVEL PROCEDIMIENTOS ---------- \n\n");
            
        if (setProcedureO.size() == 0 && setProcedureT.size() == 0)
        	System.out.println("-> NINGUNA DE LAS BD POSEEN PROCEDIMIENTOS\n\n");
        else {
        	//Se obtiene aquellos procedimientos que tiene igual nombre pero distinto perfil
        	for (Procedure p : setProcedureO) {
        		for (Procedure c : setProcedureT) {
	            	if (p.getNameProcedure().equals(c.getNameProcedure()) && !p.equals(c)) {
	            		procedureEqualsName.add(p);
	            		procedureEqualsName.add(c);
	            	}
	            }
        	} 
        	//Se obtiene los procedimientos comunes en ambas BD
        	for(Procedure p : setProcedureO) {
	            for(Procedure c : setProcedureT) 
	           		if(p.equals(c)) procedureCom.add(p);
        	}
        	//Se obtiene los procedimientos adicionales de la BD1 con respecto a las BD2
        	for(Procedure p : setProcedureO) {
	            if(!setProcedureT.contains(p))
	           		aditionalDBOne.add(p);
        	}
        	//Se obtiene los procedimientos adicionales de la BD2 con respecto a las BD1
        	for(Procedure p : setProcedureT) {
	            if(!setProcedureO.contains(p))
	           		aditionalDBTwo.add(p);
        	}
	            
        	if (procedureEqualsName.size() != 0) {
	            System.out.println("-> EXISTEN PROCEDIMIENTOS CON IGUAL NOMBRE PERO DISTINTO PERFIL, ELLOS SON:\n\n");
	            for (Procedure p : procedureEqualsName) {
	            	if (p.getTypeReturn() != null)
	            		System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "->" + p.getTypeReturn() + "\n");
	            	else
	            		System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "\n");
	            	}
	            }
	            
        	if (aditionalDBOne.size() == 0 && aditionalDBTwo.size() == 0) {
        		System.out.println("-> LOS PROCEDIMIENTO DE AMBAS BASES DE DATOS SON IGUALES, TANTO EN NOMBRE COMO PERFIL\n\n");
        		System.out.println("   -> PROCEDIMIENTOS EN COMUN: \n");
    			for (Procedure p : procedureCom) {
    				if (p.getTypeReturn() != null)
    	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "->" + p.getTypeReturn() + "\n");
    	    		else
    	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "\n");
    	    	}
        	} else {
        		System.out.println("-> NO TODO LOS PROCEDIMIENTO DE AMBAS BASES DE DATOS SON IGUALES\n\n");
        		if (procedureCom.size() != 0) {
        			System.out.println("   -> PROCEDIMIENTOS EN COMUN: \n");
        			for (Procedure p : procedureCom) {
        				if (p.getTypeReturn() != null)
        	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "->" + p.getTypeReturn() + "\n");
        	    		else
        	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "\n");
        	    	}
        		}
		            	
        		if (aditionalDBOne.size() != 0) {
        			System.out.println("-> PROCEDIMIENTOS ADICIONALES DE LA BASE DE DATOS 1: \n");
        			for (Procedure p : aditionalDBOne) {
        				if (p.getTypeReturn() != null)
        	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "->" + p.getTypeReturn() + "\n");
        	    		else
        	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "\n");
        	    	}
        		}
		            
        		if (aditionalDBTwo.size() != 0) {
        			System.out.println("-> PROCEDIMIENTOS ADICIONALES DE LA BASE DE DATOS 2: \n");
        			for (Procedure p : aditionalDBTwo) {
        				if (p.getTypeReturn() != null)
        	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "->" + p.getTypeReturn() + "\n");
        	    		else
        	    			System.out.println("   " + p.getNameProcedure() + generateSequenceParameters(p.getSetParameter()) + "\n");
        	    	}
        		}
            }
        } 
 
	}
	
	/**
	 * Metodo el cual toma dos conjuntos de tablas y chequea si existe diferencia entre las tablas
	 * @param setTableO
	 * @param setTableT
	 */
	public static void checkTable(LinkedList<Table> setTableO, LinkedList<Table> setTableT) {
		LinkedList<Table> tableCom = new LinkedList<Table>();
		LinkedList<Table> aditionalDBOne = new LinkedList<Table>();
		LinkedList<Table> aditionalDBTwo = new LinkedList<Table>();
		
		System.out.println("---------- CHEQUEO NIVEL TABLAS ---------- \n\n");
		for (Table t1 : setTableO) {
			for (Table t2 : setTableT) {
				if (t1.equals(t2)) 
					tableCom.add(t1);
				if (t1.getNameTable().equals(t2.getNameTable()) && !t1.equals(t2))
					t1.diffTablesEqualName(t2);
			}
		}
                
        //Tablas adicionales de la BD1
		for (Table t : setTableO) {
			if (!setTableT.contains(t))
				aditionalDBOne.add(t);
		}
            
            //Tablas adicionales de la BD2
		for (Table t : setTableT) {
			if (!setTableO.contains(t))
				aditionalDBTwo.add(t);
		}
            
		if (aditionalDBOne.size() == 0 && aditionalDBTwo.size() == 0) {
			System.out.println("-> LAS TABLAS DE AMBAS BASES DE DATOS SON IGUALES\n");
			System.out.println("   -> TABLAS EN COMUN: \n"); 
            for (Table t : tableCom)
            	System.out.println("   " + t.getNameTable() + "\n");
		} else {
			System.out.println("-> NO TODAS LAS TABLAS DE AMBAS BASES DE DATOS SON IGUALES\n");

           	if (tableCom.size() != 0) {
           		System.out.println("-> TABLAS EN COMUN: \n");
           		for (Table t : tableCom)
           			System.out.println("   -" + t.getNameTable() + "\n");
           	}
           	if (aditionalDBOne.size() != 0) {
           		System.out.println("-> TABLAS ADICIONALES DE LA BASE DE DATOS 1: \n");
           		for (Table t : aditionalDBOne)
           			System.out.println("   -" + t.getNameTable() + "\n");
            	}
            	if (aditionalDBTwo.size() != 0) {
            		System.out.println("-> TABLAS ADICIONALES DE LA BASE DE DATOS 2: \n");
            		for (Table t : aditionalDBTwo)
            			System.out.println("   -" + t.getNameTable() + "\n"); 
            	}
            }
		}
}
