/**
 * Clase la cual es la encargada de obtener los datos para las conecciones, establecer conexiones y generar el conjunto de tabla y procedimientos de las BD
 */

package Aux;

import java.sql.*;
import java.util.*;

import Procedure.Procedure;
import Table.Table;
import Table.Column.Column;
import Table.FKey.FKey;
import Table.Index.Index;
import Table.PKey.PKey;
import Table.Trigger.Trigger;

public class Connect {

    private String host;
    private String nameDB;
    private String user;
    private String password;

    public String getNameDB() {
        return this.nameDB;
    }

    public void requestDataDefault(String host, String nameDB, String user, String password) {
    	this.host = host;
    	this.nameDB = nameDB;
    	this.user = user;
    	this.password = password;
    }
    
    public void requestData() {
        Scanner sc = new Scanner(System.in);
        System.out.println("INGRESE EL HOST DE SU DB - Por ejemplo: localhost:3306");
        this.host = sc.nextLine();
        System.out.println("INGRESE EL NOMBRE DE LA BD A CONECTARSE:");
        this.nameDB = sc.nextLine();
        System.out.println("INGRESE EL USUARIO");
        this.user = sc.nextLine();
        System.out.println("INGRESE LA CONTRASEÑA:");
        this.password = sc.nextLine();
    }

    public Connection establishConnection(Connection connection) {
        try {
            String url = "jdbc:mysql://"+this.host+"/"+this.nameDB;
     		String username = this.user;
            String password = this.password;
            connection = DriverManager.getConnection(url, username, password);

            return connection;
        } catch (SQLException e) {
            System.out.println("-------- FALLA EN LA CONEXION --------");
			e.printStackTrace();
			return null;
        }
    }
    
    private LinkedList<Column> generateSetColumnsForTable(Connection connection, String nameTable) {
        LinkedList<Column> setColumns = new LinkedList<Column>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetColumns = metaData.getColumns(this.nameDB, null, nameTable, null);

            while(resultSetColumns.next()) {
                Column column = new Column();
                String nameColumn = resultSetColumns.getString(4);
                String typeColumn = resultSetColumns.getString(6);
                column.setNameColumn(nameColumn);
                column.setTypeColumn(typeColumn);
                setColumns.add(column);
            }

            return setColumns;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private boolean indexAndNotForeingKey(String nameIndex, LinkedList<FKey> setForeignKeys) {
    	for (FKey fk : setForeignKeys) {
    		if (fk.getNameFK().equals(nameIndex)) return false;
    	}
    	
    	return true;
    }
    
    private LinkedList<Index> generateSetIndexsForTable(Connection connection, Table table) {
    	LinkedList<Index> setIndexs = new LinkedList<Index>();
    	
    	try {
    		DatabaseMetaData metaData = connection.getMetaData();
    		ResultSet resultSetIndexs = metaData.getIndexInfo(this.nameDB, null, table.getNameTable(), false, false);
    		
    		while(resultSetIndexs.next()) {
    			boolean exist = false;
    			String nameIndex = resultSetIndexs.getString(6);
    			if (indexAndNotForeingKey(nameIndex, table.getSetForeingKeys()) && !nameIndex.equals("PRIMARY")) {
	    			for (int i = 0; i < setIndexs.size(); i++) {
	    				if (setIndexs.get(i).getNameIndex().equals(nameIndex)) {
	    					setIndexs.get(i).getNameColumns().add(resultSetIndexs.getString(9));
	    					exist = true;
	    				}
	    			}
	    			if (!exist) {
	    				Index index = new Index();
	    				LinkedList<String> nameColumns = new LinkedList<String>();
	    				nameColumns.add(resultSetIndexs.getString(9));
	    				index.setNameIndex(nameIndex);
	    				index.setIsUnique(resultSetIndexs.getBoolean(4));
	    				index.setNameColumns(nameColumns);
	    				setIndexs.add(index);
	    			}
    			}
    		}
    		
    		return setIndexs;
    	} catch (SQLException e) {
    		e.printStackTrace();
            return null;
    	}
    }

    private LinkedList<PKey> generateSetPrimaryKeysForTable(Connection connection, String nameTable) {
        LinkedList<PKey> setPrimaryKeys = new LinkedList<PKey>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetPrimaryKeys = metaData.getPrimaryKeys(this.nameDB, null, nameTable);

            PKey pk = new PKey();
        	LinkedList<String> namePK = new LinkedList<String>();
            while(resultSetPrimaryKeys.next()) {
            	namePK.add(resultSetPrimaryKeys.getString(4));
            }
            
            pk.setSetName(namePK);
            setPrimaryKeys.add(pk);
            return setPrimaryKeys;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LinkedList<String> generateSetUniqueKeysForTable(Connection connection, String nameTable) {
        LinkedList<String> setUniqueKeys = new LinkedList<String>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetUniqueKeys = metaData.getIndexInfo(this.nameDB, null, nameTable, true, false);

            while(resultSetUniqueKeys.next()) {
                if(resultSetUniqueKeys.getBoolean(4) == false) {
                    String nameUniqueKey = resultSetUniqueKeys.getString(9);
                    setUniqueKeys.add(nameUniqueKey);
                }
            }

            return setUniqueKeys;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private LinkedList<FKey> generateSetForeignKeyForTable(Connection connection, String nameTable) {
    	LinkedList<FKey> setForeignKeys = new LinkedList<FKey>();
    	
    	try {
    		DatabaseMetaData metaData = connection.getMetaData();
    		ResultSet resultSetForeignKey = metaData.getImportedKeys(this.nameDB, null, nameTable);
    		
    		while(resultSetForeignKey.next()) {
    			boolean exist = false;
    			String nameFK = resultSetForeignKey.getString(12);
    			for (int i = 0; i < setForeignKeys.size(); i++) {
    				if (setForeignKeys.get(i).getNameFK().equals(nameFK)) {
    					setForeignKeys.get(i).getNameColumnFK().add(resultSetForeignKey.getString(8));
    					setForeignKeys.get(i).getNameColumnPK().add(resultSetForeignKey.getString(4));
    					exist = true;
    				}
    			}
    			if (!exist) {
    				FKey fk = new FKey();
    				LinkedList<String> nameColumnFK = new LinkedList<String>();
    				LinkedList<String> nameColumnPK = new LinkedList<String>();
    				nameColumnFK.add(resultSetForeignKey.getString(8));
    				nameColumnPK.add(resultSetForeignKey.getString(4));
    				fk.setNameFK(nameFK);
    				fk.setNameTableRef(resultSetForeignKey.getString(3));
    				fk.setNameColumnFK(nameColumnFK);
    				fk.setNameColumnPK(nameColumnPK);
    				setForeignKeys.add(fk);
    			}
    		}
    		
    		return setForeignKeys;
    	} catch (SQLException e) {
            e.printStackTrace();
            return null;
    	}
    }

    private LinkedList<Trigger> generateSetTriggerForTable(Connection connection, String nameTable) {
        LinkedList<Trigger> setTrigger = new LinkedList<Trigger>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT TRIGGER_NAME, ACTION_TIMING, EVENT_MANIPULATION FROM information_schema.TRIGGERS WHERE EVENT_OBJECT_TABLE = "+"'"+nameTable+"'"+"AND TRIGGER_SCHEMA = "+"'"+this.nameDB+"'";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                Trigger trigger = new Trigger();
                trigger.setNameTrigger(resultSet.getString("TRIGGER_NAME"));
                trigger.setMomentTrigger(resultSet.getString("ACTION_TIMING"));
                trigger.setEventTrigger(resultSet.getNString("EVENT_MANIPULATION"));
                setTrigger.add(trigger);
            }

            return setTrigger;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LinkedList<Table> generateSetTables(Connection connection, String name) {
        LinkedList<Table> setTables = new LinkedList<Table>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetTables = metaData.getTables(name, null, "%", null);

            while(resultSetTables.next()) {
                Table table = new Table();
                String nameTable = resultSetTables.getString(3);
                table.setNameTable(nameTable);

                table.setSetColumn(generateSetColumnsForTable(connection, nameTable));
                table.setSetPrimaryKeys(generateSetPrimaryKeysForTable(connection, nameTable));
                table.setSetUniqueKeys(generateSetUniqueKeysForTable(connection, nameTable));
                table.setSetForeingKeys(generateSetForeignKeyForTable(connection, nameTable));
                table.setSetIndexs(generateSetIndexsForTable(connection, table));
                table.setSetTrigger(generateSetTriggerForTable(connection, nameTable));
                setTables.add(table);
            }

            return setTables;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private LinkedList<Pair> generateSetParameter(Connection connection, String nameProcedure) {
        LinkedList<Pair> setParameter = new LinkedList<Pair>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT DATA_TYPE, PARAMETER_MODE FROM information_schema.PARAMETERS WHERE SPECIFIC_SCHEMA = "+"'"+nameDB+"' AND SPECIFIC_NAME = "+"'"+nameProcedure+"' AND ORDINAL_POSITION != 0";
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
            	Pair p = new Pair();
            	p.setFirst(resultSet.getString("PARAMETER_MODE"));
                p.setSecond(resultSet.getString("DATA_TYPE"));
                setParameter.add(p);
            }

            return setParameter;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getTypeReturnProcedure(Connection connection, String nameProcedure) {
        String typeReturn = null;;

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT DATA_TYPE FROM information_schema.PARAMETERS WHERE SPECIFIC_SCHEMA = "+"'"+nameDB+"' AND SPECIFIC_NAME = "+"'"+nameProcedure+"' AND ORDINAL_POSITION = 0";;
            ResultSet resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                typeReturn = resultSet.getString("DATA_TYPE");
            }

            return typeReturn;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LinkedList<Procedure> generateSetProcedureAndFunction(Connection connection, String name) {
        LinkedList<Procedure> setProcedure = new LinkedList<Procedure>();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSetProdecure  = metaData.getProcedures(name, null, "%");

            while(resultSetProdecure.next()) {
                Procedure procedure = new Procedure();

                String nameProcedure = resultSetProdecure.getString(9);
                procedure.setNameProdecure(nameProcedure);

                procedure.setSetParameter(generateSetParameter(connection, nameProcedure));
                procedure.setTypeReturn(getTypeReturnProcedure(connection, nameProcedure));

                setProcedure.add(procedure);
            }

        return setProcedure;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
