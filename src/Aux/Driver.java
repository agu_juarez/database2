/**
 * Clase la cual es la encargada de cargar los Driver para la conexion a BD MySql
 */

package Aux;

import java.sql.*;
import java.util.*;

public class Driver {
	
	public static void loadDriver() {
        System.out.println("-------- Conexion A La Base De Datos Mysql ------------");
        try {
            String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver);
        } catch (ClassNotFoundException e) {
            System.out.println("HUBO UN PORBLEMA EN LA CARGA DEL DRIVER");
			e.printStackTrace();
			return;
        }
        System.out.println("-------- CARGA DEL DRIVER EXITOSA --------");
    }
	
}
