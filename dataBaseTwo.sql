create database dataBaseTwo;
use dataBaseTwo;

drop table if exists vehiculo;
drop table if exists persona;
drop table if exists duenio;
drop table if exists parquimetro;
drop table if exists estacionamiento;

create table `vehiculo` (
	`nro_patente` varchar(7) not null,
    `marca`       varchar(15) not null,
    `modelo`      varchar(15) not null,
    `color`       enum('gris', 'negro', 'azul'),
    `saldo_actual`varchar(30),
    constraint pkvehiculo primary key (nro_patente)
);

create table `persona` (
	`dni`             varchar(40) not null,
    `nombre_apellido` varchar(45),
    `direccion`       varchar(30) not null,
    `telefono`		  varchar(15) not null,
    constraint pkpersona primary key (dni)
);

create table `duenio` (
	`dni`         varchar(40) not null,
	`nro_patente` varchar(7) not null,
    constraint pkduenioAuto primary key (nro_patente, dni),
    constraint fkvehiculo foreign key (nro_patente) references `vehiculo` (`nro_patente`),
    constraint fkpersona foreign key (dni) references `persona` (`dni`)
);

create table `parquimetro` (
	`nro_parquimetro` integer not null,
    `calle`           varchar(20) not null,
    `altura`          integer not null,
    constraint pkparquimetro primary key (nro_parquimetro),
    constraint secu_parquimetro unique (nro_parquimetro)
);

delimiter $
create trigger check_insert_altura before insert on parquimetro
for each row
begin
	if (new.altura <= 0 || new.altura > 5000) then
		 signal sqlstate '45000' set message_text = 'Altura invalida al insertar';
	end if;
end $

delimiter $
create trigger check_update_altura after insert on parquimetro
for each row
begin
	if (new.altura <= 0 || new.altura > 5000) then
		 signal sqlstate '45000' set message_text = 'Altura invalida al actualizar';
	end if;
end $

create table `estacionamiento` (
	`nro_estacionamiento` integer auto_increment,
    `nro_patente`		  varchar(7) not null,
    `nro_parquimetro`     integer not null,
    `fecha`				  date,
    `saldo_inicio`		  integer,
    `saldo_final`         integer,
    `hora_entrada`        time,
    `hora_salida`         time,
    constraint pkestacionamiento primary key (nro_estacionamiento),
    constraint fkpatentevehiculo foreign key (nro_patente) references `vehiculo` (`nro_patente`) on delete restrict,
    constraint fkparquimetro foreign key (nro_parquimetro) references `parquimetro` (`nro_parquimetro`)
);